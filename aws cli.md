AWS Command Line Interface
---
**WORK WITH SECURITY GROUP**

**List security group** <br>
`aws ec2 describe-security-groups` <br>

**List a specific security groups** <br>
`aws ec2 describe-security-groups --group-ids sg-088f788901f741b1d` <br>

**List VPC** <br>
`aws ec2 describe-vpcs` <br>

**Create a security groups** <br>
` aws ec2 create-security-group --group-name my-sg --description "My SG" --vpc-id vpc-902797e9` <br>

**Assign ingress rule to a security group** <br>
`aws ec2 authorize-security-group-ingress --group-id sg-088f788901f741b1d --protocol tcp --port 22 --cidr 0.0.0.0/0` <br>

**WORK WITH KEY PAIR** <br>

**Create a Key pair and download the pem** <br>
`aws ec2 create-key-pair --key-name Kpcli --query 'keyMaterial' --output text > MyKpcli.pem` <br>
