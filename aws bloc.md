SOLUTION ARCHITECT**

SOLUTION ARCHITECT

AWS EC2 Comparison

https://instances.vantage.sh/

——————————————————————

WELL ARCHITECTED FRAMEWORK

Operations

Security

Reliability

Performance

Costs

Docs.aws.amazon.com/cli/latest

——————————————————————

EC2 Instances Launch Types

- On-demand

Paie pour uniquement ce qu’on consomme (Pay as you go)

Pour de courtes charges de travail d’un temps défini

Facturé à la seconde avec un minimum de une minute pour du Linux et une heure pour du windows

No upfront paiement

- Reserved instances

    Standard Reserved instances

Instances réservé pour un minimum d’un an.

Elle fournit jusqu’à 75% de remise comparé à l’offre à la demande

    Convertible reserved instances

Instances réservé pour un minimum d’un an.

Cependant le type d’instance réservé peut être changé.

Elle offre jusqu’à 54% de remise comparé à l’offre à la demande

    Scheduled reserved instances

Instances réservé pour une période minimum d’un an sur une fenêtre horaire défini

- Spot instances

Pour de courtes charge de travail.

Elle offre jusqu’à 90% de remise Cependant l’instance peut être récupéré par AWS lorsque le prix de l’instance (instance_price) dépasse le prix proposé par le client (client_price)

Lorsque l’instances est redemandé, on a 2 minutes avant qu’elle soit reprise

On peut soit la stopper afin de continuer notre activité lorsque (client_price) > (instance_price) Ou carrément la terminer.

Ou alors, lorsque qu’on ne souhaite pas que l’instance nous soit reprise on peut utiliser Les SPOT BLOCK

Les spot block permettent de conserver une instance spot pour une durée de 1 à 6h sans interruptions

SPOT FLEET ??????????????????????????????????????

- Dedicated Host

Allocation du serveur entier pour une période de trois ans

Adéquat lorsqu’on a certaines normes de conformité à respecter

Possibilité de BYOL (Bring Your Own License)

- Dedicated instances

Instances alloué sur un Hôte dédié

————————————————————————

EC2 Instances Types

Ec2instances.info

R : Application that need a lot of RAM - in memory cache

C : Application that need a lot of CPU - Compute / Database

M : Medium between R & C - general application

I : Application that need a good I/O - database

G : Application that need GPU - video rendering / machine learning

T2/T3 : burstable instances () ??????????????????????????????????????

————————————————————————

Placement Groups

Les placements groups sont une manière de dire à AWS comment est ce qu’on souhaiterait que nos instances soient distribués à travers le système.

Il y a :

- Cluster

Le type cluster place les instances sur le même rack dans la même zone de disponibilité

Il fournit une une connexion low-latency

Cependant en cas de panne toutes les instances sont perdues

N’est pas compatible avec tout Type d’instance

- Spread

Le type spread place les instances dans différentes AZ

Il permet un maximum de 7 instances par AZ

Il permet une haute disponibilité

- Partitions

Le type partition place les instances sur des partitions dans des AZ

On a un maximum de 7 partitions par AZ et 100 instances par partitions

Les pannes d’une partitions n’affectent pas les autres partitions

————————————————————————

HIGH AVAILABILITY & SCALIBILITY

Scalabilité horizontale = élasticité

Haute disponibilité passive = scalabilité verticale

Haute disponibilité active = scalabilité horizontale

Scalabilité verticale : augmente/diminue la taille de l’instance (scale up/down)

Scalabilité horizontale : augmente/diminue le nombre d’instances (scale out/in)

LOAD BALANCER

Le load balancer permet de distribuer le trafic entrant à travers différentes instances

Le statut de ces instances est checké sur un laps de temps défini

Si l’instance est saine, elle retourne 200 (OK)

Load Balancer Stickiness ensures traffic is sent to the same backend instance for a client. This helps maintaining session data

SNI (Server Name Indication) is a feature allowing you to expose multiple SSL certs if the client supports it

0798472625

On a trois types de Load Balancer :

- Classic Load Balancer (v1 - old) 2009

HTTP, HTTPS, TCP

- Application Load Balancer (v2 - new) 2016

HTTP, HTTPS, Websocket

- Network Load Balancer (v2 - new) 2017

TCP, TLS (Secure TCP), UDP

Quand on souhaite une adresse IPV4 statique ou d’extrême performance

Temps de latence de 100ms (400ms pour ALB)

Network Load Balancers expose a public static IP, whereas an Application or Classic Load Balancer exposes a static DNS (URL)

X-forwarded-for est une entête contenant l’adresse IP du client accédant à notre site internet à travers le load balancer

AUTO SCALING GROUP

L’auto scaling permet le provisionnement ou la suppression automatique d’instances selon un seuil défini

Il intervient dans le processus de haute disponibilité d’une infrastructure

Target tracking

Définit un seuil de métric à partir de quoi scale in OU scale out

Step / Simple scaling

Définit une alarme de métrique avec CloudWatch à partir de quoi scale in OU scale out

Scheduled scaling

Programme (cron) un scaling à une date défini

————————————————————————

EBS & EFS

Mount an EBS Volume (https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html)

Can be connected to one ec2 instances and is locked in only one AZ

VOLUME TYPE

gp2: general purpose volumes (cheap)

1 Gib - 16 Tib

3 iops / Gib

min 100 iops burst to 3000, max 16000 iops

La taille du disque et les iops sont liés

io1: provisionned iops ssd (io1)

4 Gib / 16 Tib

min 100 iops, max 16 Tib

La taille du disque et les iops ne sont pas liés

St1: Throughput optmized HDD

500 Gib - 16 Tib, 500Mib/s Throughput

Sc1: Cold HDD, infrequently accessed data

500 Gib - 16 Tib, 500Mib/s Throughput

RAID 0 : performance

RAID 1 Or Mirror : Fault tolerant

MOUNT AN EFS ON EC2

An Efs can mount 100 instances across AZ

It supports only Linux (Because it is a POSIX file system)

Install on the instance : amazon-efs-utils package (https://docs.aws.amazon.com/efs/latest/ug/installing-amazon-efs-utils.html)

Create directory

INSTANCE STORE

Instance store is a physical hard drive mount on the server

It allows llow latency and maximum amount of io

Losed when the instance is losed ( ephemeral drive )

————————————————————————

RDS + AURORA + ELASTICACHE

RDS

Relational Database Service

Managed DB service using SQL as query language

It allows to create database in the cloud such as :

    Postgres
    MySQL
    MariaDB
    Oracle
    Microsoft SQL server
    Aurora (AWS proprietary database)

It has :

    Continuous backup and point in time restore ( there is a 7 days backup by default but it can be increased to 35 days )
    DB Snapshot ( manually triggered by the user and the retention is as long as we want )

RDS Read Replicas

We can create up to 5 read replicas in the same AZ, cross AZ or Cross region

There is a replication between the main database and the read replicas

Read replicas can be promoted to their own database

Read replicas are used for SELECT

In AWS there’s a network cost when data goes from one region to another

Read replicas can be set up as multi AZ

Read replicas => Async replication

RDS Multi AZ

Create a standby instance of the main database in case of failure

There is a DNS name between database and application to perform automatic failover

Increase availability

Multi AZ => Sync replication

AURORA

Proprietary technology of AWS

PostgreSQL and MySQl are both supported by Aurora

Aurora is 5x performant than RDS on Mysql and 3x than Postgre on RDS

Aurora storage automatically grows in increments of 10Gb up to 64TB

Aurora can have 15 replicas and the replication process is faster

Aurora has automatic failover and is HA native

Aurora is more expensive than RDS

AURORA High availability

Aurora store 6 copies of your data across 3AZ

07

ELASTICACHE

Managed Cache

Read from the cache instead of reading from the database

Do not support IAM authentication

ELASTICACHE REDIS VS MemCached

REDIS is multi AZ, read replicas, data durability, backup and restore feature

While Memcached is a pure cache, non persistent, no backup and restore, multi-node for partitioning data (sharing)

————————————————————————

ROUTE 53

DNS Managed service

A : hostname to IPV4

AAAA : hostname to IPV6

CNAME : hostname to hostname (not free, not applicable to root domain)

Alias : hostname to AWS resource (free, applicable on root domain)

ROUTING POLICY

    Simple Policy : The browser choose one of the response returned by the DNS
    Weighted Policy : Assign percentage of trafic to instances
    Latency : Redirect traffic to the point (region or AZ) with the less latency
    Health Check :
    Failover :
    Geolocation :
    Multivalue :

————————————————————————

ARCHITECTURE DISCUSSION

WELL ARCHITECTED FRAMEWORK

Cost :

Performance :

Reliability :

Security :

Operational excellence :

————————————————————————

S3

AWS service to store object, permit versioning and encryption …

S3 encryption

SSE-S3 : encryption using keys handled & managed by S3

x-amz-server-side-encryption : AES256

SSE-KMS : encryption using keys handled & managed by KMS

x-amz-server-side-encryption : aws:kms

SSE-C : encryption using keys provided by customer

Client side encryption : Encrypt the object before sending it to aws

S3 has versioning, replication accross region or in same region

S3 can register logs, select another bucket for registering logs

S3 has different storage:

Standard

IA

One zone IA

Intelligent tiering

Glacier

expedited (1 à 5 min)

standard (3 à 5h)

bulk (5 à 12h)

Deep Glacier

We can define lifecycle rules

We can generate a presigned url (only in CLI) for accessing temporary a private S3 bucket

S3 has notifications event through SNS, SQS, topic

————————————————————————

STORAGE RESUME

S3 : Object Resume

Glacier : Object Archival

EFS : Network file system for linux

Fsx For Windows : Network file system for windows

Fsx For lustre : Network file system for linux cluster

EBS volumes : Network storage for one EC2 instances

Instances storage : Physical storage for EC2 instances

Storage Gateway : Allow cloud access from on-premise (File Gateway, Volume Gateway, Tape Gateway)

Snowball / Snowmobile : To move large amount of data to the cloud

Database : For specific workloads, usually with indexing and querying

————————————————————————

TO NOT FORGET

FOR EC2 TO KNOW ABOUT IT SELF : http://169.254.169.254

AWS policy generator

AWS policy simulator

————————————————————————

ATHENA

Athena is to query data in S3 without loading in a database using the SQL language

————————————————————————

CloudFront

CloudFront signed URL provide access to one file

CloudFront signed Cookies provide access to hundreds of files

————————————————————————

DECOUPLING APPLICATIONS : SQS-SNS-KINESIS-ACTIVE MQ

SQS

Max size CIDR on AWS : /16

Min size CIDR on AWS : /28

S3 and DynamoDb have a Gateway Endpoint

All the other ones have Interface endpoint

RPO : Time before disaster

RTO : Time after disaster

Backup & Restore (snapshot …)

Pilot Light (Les applications critique sont déjà mis en sécurité)

Warm standby (des instances de récupérations minime existent déjà, elles peuvent être scalé)

Multi Site / Hot site approach (Production on premise and on AWS)

AWS Multi Region

Redis

Memcached

DAX

Pour alléger les read sur la base de données

EXCELLENCE OPERATIONNELLE

SECURITE

RELIABILITE

PERFORMANCE EFFICIENCY

COST OPTIMIZATION

S3

Dynamo DB

RDS

AURORA
EMR

Redshift

CloudFront

Api gateway

Storage Gateway

EFS

Fsx

CloudFormation

ECS

ALB

EC2

SQS

SNS

Cloudwatch

Redis

Route 53

https://aws.amazon.com/fr/getting-started/fundamentals-core-concepts/. Fondamentaux AWS



